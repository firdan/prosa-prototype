var app = new Vue({
  el: '#app',
  vuetify:new Vuetify({
    theme: {
      themes: {
        light: {
          primary: '#0cac90',
          secondary: '#b0bec5',
          accent: '#8c9eff',
          error: '#b71c1c',
        },
      },
    },
  }),

  data() {
    return {
      backlog:[],
      isDrag:false,
      done:[],
      todo:[],
      colors:['red', 'pink', 'purple', 'blue', 'green', 'yellow','teal','deep-purple', 'indigo'],
      drawer:true,
      message: 'Hello Vue!',
      users:[],
      addDialog:false,
      addTaskLabel:'',
      current:{picture:{}, name:{}},
      topMember:[],
      menuItems:[
        {title:'Menu'},
        {title:'My Tasks'},
        {title:'Notification', badge:3}
      ],
      menuTeams:[],
      dateRules: [v=> /\d{4}-\d{2}-\d{2}/.test(v) || 'Date must use YYYY-MM-DD'],
      form:{
        "issue_id": "",
        "title": "",
        "assignee": "",
        "start_date": "",
        "end_date": "",
        "tags": []
      },
    }
  },

  created(){
    let raw_users = localStorage.getItem("users")
    if (raw_users){
      console.log('DATA STATUS: using localStorage')
      this.users = JSON.parse(raw_users)
      this.setInitialValues()
    } else {
      axios.get('https://randomuser.me/api/?results=70').then(response=>{
        console.log('DATA STATUS: call server randomuser')
        let res = response.data
        this.users = res.results
        localStorage.setItem('users',JSON.stringify(res.results))
        this.setInitialValues()
      })
    }

    fetch('dummy/backlog.json').then(response=> response.json()).then(json=>{
      this.backlog = json
      console.log(this.backlog)
    })
    fetch('dummy/done.json').then(response=> response.json()).then(json=>{
      this.done = json
      console.log(this.done)
    })
    fetch('dummy/todo.json').then(response=> response.json()).then(json=>{
      this.todo = json
      console.log(this.todo)
    })
  },

  computed:{
    dragOptions() {
      return {
        animation:0,
        group: "task",
        ghostClass: "ghost",
        dragClass:"drag",
        chosenClass:"chosen",
        forceFallback:true,
      };
    },
  },

  methods:{
    addNewTask(){
      this.form.issue_id = Math.floor(Math.random * 100)
      this.form.tags=this.form.tags.split(',')
      switch(this.addTaskLabel){
        case "todo":
          this.todo.push(this.form)
          break
        case "done":
          this.done.push(this.form)
          break
        case "backlog":
          this.backlog.push(this.form)
          break
      }

      //clear form
      this.form = {
        "issue_id": "",
        "title": "",
        "assignee": "",
        "start_date": "",
        "end_date": "",
        "tags": []
      }

      //close dialog
      this.addDialog = false
    },

    substractDate(date1,date2){
      date1 = moment(date1)
      date2 = moment(date2)
      return date2.diff(date1, 'days')
    },

    randomizeColor(text){
      let seed = text.length > this.colors.length ? this.colors.length : text.length
      return this.colors[seed]
    },

    randomizeUser(x){
      let results=[]
      while(results.length < x){
        let idx = Math.floor(Math.random() * this.users.length)
        let user = this.users[idx]
        if (results.length == 0){
          results.push(user)
        }else if (results.map(function(e){return e.phone}).indexOf(user.id.value) === -1) {
          results.push(user)
          console.log(user.phone)
        }
      }
      return results
    },

    setInitialValues(){
      this.current = this.users[0]
      this.topMember = this.randomizeUser(3)
      this.menuTeams=[
        {title:'Researcher', users:this.randomizeUser(4)},
        {title:'FE/BE Team', users:this.randomizeUser(2)},
        {title:'PM Team', users:this.randomizeUser(3)}
      ]
    }
  },

})
